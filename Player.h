#pragma once
#include "GameData.h"
#include "Ball.h"

class Player
{
public:
	void displayPlayers(GameData& gdata, sf::Sprite p1pad, sf::Sprite p2pad);
	void player1Movement(sf::Sprite& p1);	//movement for p1
	void player2Movement(sf::Sprite& p2);	//movement for p2
	void player1BallCollision(sf::Sprite& ball, sf::Sprite p1, Ball& ball_);					//ball collision for p1
	void player2BallCollision(sf::Sprite& ball, sf::Sprite p2, Ball& ball_);
	bool spriteintersects(const sf::Sprite & spr1, const sf::Sprite & spr2);
	bool player2Serve = false;
	sf::Sprite getPlayer1();
	sf::Sprite getPlayer2();
	~Player();
private:
	sf::Texture p1Paddle;
	sf::Texture p2Paddle;
	sf::Sprite createPlayer1();
	sf::Sprite createPlayer2();
	GameData gdata;
	const int moveDistance = 5;
};